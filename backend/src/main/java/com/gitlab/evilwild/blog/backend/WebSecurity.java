package com.gitlab.evilwild.blog.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.gitlab.evilwild.blog.backend.service.UserDetailsServiceImpl;

import lombok.AllArgsConstructor;

@EnableWebSecurity
@AllArgsConstructor
public class WebSecurity extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsServiceImpl userDetailsService;

		@Bean
		CorsConfigurationSource corsConfigurationSource() {
			UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			CorsConfiguration configuration = new CorsConfiguration();
			configuration.addAllowedOrigin("http://localhost:3000");
			configuration.addAllowedHeader("*");
			configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));
			source.registerCorsConfiguration("/**", configuration);
			return source;
		}

		@Override
		public void configure(AuthenticationManagerBuilder builder) throws Exception {
			builder.userDetailsService(userDetailsService);
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.cors().configurationSource(corsConfigurationSource())
				.and()
				.csrf().disable()
				.authorizeRequests()
						.antMatchers(HttpMethod.DELETE).authenticated()
						.antMatchers(HttpMethod.PUT).authenticated()
						.antMatchers("/**").permitAll()
						.and().httpBasic();
		}
}
