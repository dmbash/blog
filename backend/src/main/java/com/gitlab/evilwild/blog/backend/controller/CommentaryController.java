package com.gitlab.evilwild.blog.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.evilwild.blog.backend.entity.CommentaryEntity;
import com.gitlab.evilwild.blog.backend.service.CommentaryService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
class CommentaryController implements ICrudController<CommentaryEntity, Long>{
	@Autowired
	private final CommentaryService commentaryService;

	@Override
	@GetMapping("/commentary/{id}")
	public CommentaryEntity get(@PathVariable Long id) {
		return commentaryService.getById(id);
	}

	@Override
	@PutMapping("/commentary/{id}")
	public CommentaryEntity update(@RequestBody CommentaryEntity newCommentary, @PathVariable Long id) {
		return commentaryService.update(newCommentary, id);
	}

	@Override
	@DeleteMapping("/commentary/{id}")
	public void delete(@PathVariable Long id) {
		commentaryService.delete(id);
	}

	@Override
	public CommentaryEntity create(CommentaryEntity entity) {
		return null;
	}

	@Override
	public List<CommentaryEntity> getAll() {
		return null;
	}
}
