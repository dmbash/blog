package com.gitlab.evilwild.blog.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.evilwild.blog.backend.entity.CycleEntity;
import com.gitlab.evilwild.blog.backend.service.CycleService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
class CycleController implements ICrudController<CycleEntity, Long>{
	@Autowired
	private final CycleService cycleService;

	@Override
	@PostMapping("/cycle")
	public CycleEntity create(@RequestBody CycleEntity cycle) {
		return cycleService.create(cycle);
	}

	@Override
	@PutMapping("/cycle/{id}")
	public CycleEntity update(@RequestBody CycleEntity newCycle, @PathVariable Long id) {
		return cycleService.update(newCycle, id);
	}

	@Override
	@DeleteMapping("/cycle/{id}")
	public void delete(@PathVariable Long id) {
		cycleService.delete(id);
	}

	@Override
	@GetMapping("/cycle/{id}")
	public CycleEntity get(@PathVariable Long id) {
		return cycleService.getById(id);
	}

	@Override
	@GetMapping("/cycles")
	public List<CycleEntity> getAll() {
		return cycleService.getAll();
	}
}
