package com.gitlab.evilwild.blog.backend.controller;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
class Error {
	String message;
}

@RestController
@RestControllerAdvice
public class ErrorControllerImpl {

	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(EntityNotFoundException.class)
	public Error handleEntityNotFound(final EntityNotFoundException e) {
		return new Error("Entity not found");
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public Error handleUnknownException(final Exception e) {
		return new Error("Server error");
	}
}
