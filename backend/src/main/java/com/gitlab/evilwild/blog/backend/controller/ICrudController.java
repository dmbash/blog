package com.gitlab.evilwild.blog.backend.controller;

import java.io.Serializable;
import java.util.List;

import com.gitlab.evilwild.blog.backend.entity.AbstractEntity;

public interface ICrudController<T extends AbstractEntity, K extends Serializable> {
	void delete(K id);
	T create(T entity);
	T update(T newEntity, K id);
	T get(K id);
	List<T> getAll();
}
