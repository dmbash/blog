package com.gitlab.evilwild.blog.backend.controller;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.evilwild.blog.backend.entity.UserEntity;
import com.gitlab.evilwild.blog.backend.service.UserService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@AllArgsConstructor
public class UserController {

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private final UserService userService;

	@PostMapping("/auth")
	String auth(@RequestBody UserEntity loggingUser) {
		UserEntity user = userService.getByUsername(loggingUser.getUsername());

		if (user != null) {
			if (passwordEncoder.matches(loggingUser.getPassword(), user.getPassword())) {
				return "Basic " + Base64.getEncoder().encodeToString((loggingUser.getUsername() + ":" + loggingUser.getPassword()).getBytes());
			}

			return "Invalid password";
		}

		return "User doesn't exist";
	}

	@PostMapping("/signup")
	String singnup(@RequestBody UserEntity user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		log.info("[SIGNUP]: created new user: " + user.getUsername());
		userService.create(user);
		return "User created";
	}

}
