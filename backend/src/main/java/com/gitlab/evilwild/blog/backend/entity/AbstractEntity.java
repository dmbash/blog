package com.gitlab.evilwild.blog.backend.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@MappedSuperclass
@Accessors(chain = true)
@Data
@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {
	private static final long serialVersionUID = 7306238646124482688L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@CreationTimestamp
	Date createdAt;

	@UpdateTimestamp
	Date modifiedAt;
}
