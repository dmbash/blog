package com.gitlab.evilwild.blog.backend.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="article")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class ArticleEntity extends AbstractEntity {
	private static final long serialVersionUID = -2135227823776571516L;

	@Column(name="article_title", nullable = false)
	private String title;

	@Column(name="article_text", nullable = false)
	String text;

	@OneToMany
	List<CommentaryEntity> comments;

	@ManyToOne
	@JsonIgnoreProperties("articles") // remove recursive articles in nested cycles
	CycleEntity cycle;

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	RatingEntity rating = new RatingEntity();

	@OneToOne(cascade = CascadeType.ALL, optional = false)
	ViewsEntity views = new ViewsEntity();

	@ManyToMany
	@JsonIgnoreProperties("articles") // remove recursive articles in nested cycles
	Set<ThemeEntity> themes;

	public ArticleEntity(String title, String text) {
		super();
		this.title = title;
		this.text = text;
	}
}
