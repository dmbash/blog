package com.gitlab.evilwild.blog.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="commentary")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class CommentaryEntity extends AbstractEntity {
	private static final long serialVersionUID = -8117632084042080136L;

	@Column(name="commentary_author")
	String author;

	@Column(name="commentary_text")
	String text;

	public CommentaryEntity(String author, String text) {
		this.author = author;
		this.text = text;
	}

}
