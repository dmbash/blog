package com.gitlab.evilwild.blog.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Accessors(chain = true)
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name="users")
public class UserEntity extends AbstractEntity {

	private static final long serialVersionUID = 3354987252351556554L;

	@Column(name="username", unique = true)
	@Setter(AccessLevel.NONE) String username;

	@Column(name="password", nullable = false)
	String password;
}
