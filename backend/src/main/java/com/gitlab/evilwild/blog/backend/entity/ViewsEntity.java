package com.gitlab.evilwild.blog.backend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name="views")
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
public class ViewsEntity extends AbstractEntity {

	private static final long serialVersionUID = 7669831851004984027L;

	@Column(name="views_count", columnDefinition = "integer default 0")
	int count;
}
