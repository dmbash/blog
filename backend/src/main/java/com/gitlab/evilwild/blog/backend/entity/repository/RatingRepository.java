package com.gitlab.evilwild.blog.backend.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.RatingEntity;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {

}
