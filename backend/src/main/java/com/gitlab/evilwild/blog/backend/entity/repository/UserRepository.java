package com.gitlab.evilwild.blog.backend.entity.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gitlab.evilwild.blog.backend.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, String> {
	Optional<UserEntity> findUserByUsername(String username);
}
