package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.ArticleEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.ArticleRepository;

@Service
public class ArticleService extends AbstractService<ArticleEntity, ArticleRepository, Long>{

	@Autowired
	public ArticleService (final ArticleRepository repository) {
		super(repository);
	}

	@Override
	public ArticleEntity update(ArticleEntity editedArticle, Long id) {
		return repository.findById(id)
				.map(article -> {
					article
					.setTitle(editedArticle.getTitle())
					.setThemes(editedArticle.getThemes())
					.setCycle(editedArticle.getCycle())
					.setText(editedArticle.getText());
					return repository.save(article);
				})
				.orElseThrow(() -> new EntityNotFoundException());
	}
}
