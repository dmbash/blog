package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.RatingEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.RatingRepository;

@Service
public class RatingService extends AbstractService<RatingEntity, RatingRepository, Long>{

	public RatingService(final RatingRepository repository) {
		super(repository);
	}

	@Override
	public RatingEntity update(RatingEntity updatedRating, Long id) throws EntityNotFoundException {
		return repository.findById(id)
				.map(rating -> {
					rating
					.setGood(updatedRating.getGood())
					.setBad(updatedRating.getBad());
					return repository.save(rating);
				})
				.orElseThrow(() -> new EntityNotFoundException());
	}


}
