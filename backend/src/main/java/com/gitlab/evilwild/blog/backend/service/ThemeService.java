package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.ThemeEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.ThemeRepository;

@Service
public class ThemeService extends AbstractService<ThemeEntity, ThemeRepository, Long> {

	public ThemeService(final ThemeRepository repository) {
		super(repository);
	}

	@Override
	public ThemeEntity update(ThemeEntity newTheme, Long id) {
		return repository.findById(id)
				.map(theme -> {
					theme
						.setTitle(newTheme.getTitle());
					return repository.save(theme);
				})
				.orElseThrow(() -> new EntityNotFoundException());
	}

}
