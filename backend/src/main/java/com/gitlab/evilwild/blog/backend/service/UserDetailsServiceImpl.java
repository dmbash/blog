package com.gitlab.evilwild.blog.backend.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.UserEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.UserRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity user = repository.findUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));

		if (user == null) {
			log.info("[AUTH]: User wasn't found ");
			throw new UsernameNotFoundException("[AUTH]: User not found");
		}

		List<SimpleGrantedAuthority> grantedAuthorities = Arrays.asList(new SimpleGrantedAuthority("admin"));

		log.info("[AUTH]: User was found " + user.getUsername());

		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
	}

}
