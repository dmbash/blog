package com.gitlab.evilwild.blog.backend.service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.gitlab.evilwild.blog.backend.entity.UserEntity;
import com.gitlab.evilwild.blog.backend.entity.repository.UserRepository;

@Service
public class UserService extends AbstractService<UserEntity, UserRepository, String> {
	public UserService(final UserRepository repository) {
		super(repository);
	}

	@Override
	public UserEntity update(UserEntity entity, String id) throws EntityNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(rollbackOn = EntityNotFoundException.class)
	public UserEntity getByUsername(String username) {
		return repository.findUserByUsername(username).orElseThrow(() -> new EntityNotFoundException());
	}
}
