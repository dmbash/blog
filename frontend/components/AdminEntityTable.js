import React, { useState } from "react";
import { useRouter } from 'next/router';
import axios from "../helpers/axios";
import styles from "../styles/AdminEntityTable.module.scss";
import AppButton from "../components/AppButton";
import _ from "lodash";

// supply each field with optional filter, filter is function, that applied in interpolation, e.g. stripText(text, length)
const AdminEntity = (props) => {
  const { data, isActive, click } = props;

  return (
    <tr
      onClick={click}
      className={`${styles.Entity} ${isActive ? styles.ActiveEntity : ""}`}
    >
      {Object.keys(data).map((key) => {
        const row = data[key];
        return <td key={key}>{row.filter(row.value)}</td>;
      })}
    </tr>
  );
};

// columns = array with names of entities fields to include
const AdminEntityTable = (props) => {
  const router = useRouter();
  const {
    columns,
    header,
    emptyMessage,
    newLink,
    newButtonText,
    loadNewPageLink,
    isPageble,
    editLink,
    deleteLink,
  } = props;
  let [activeEntityId, setActiveEntityId] = useState(null);
  let [entities, setEntities] = useState(props.entities);
  let [entitiesPage, setEntitiesPage] = useState(1);

  const loadNewPage = (e) => {
    e.preventDefault();
    axios.get(loadNewPageLink(entitiesPage))
      .then((res) => {
        const { data } = res;
        setEntities(entities.concat(data));
        setEntitiesPage(entitiesPage + 1);
      })
      .catch((err) => {
        console.error(err);
      })
  };

  const onEditClick = (e) => {
    e.preventDefault();

    router.push(editLink(activeEntityId));
  }

  const onDeleteClick = (e) => {
    e.preventDefault();

    const response = confirm("Вы уверены?");
    if (response) {
      const link = deleteLink(activeEntityId);
      axios
        .delete(link)
        .then(() => {
          setActiveEntityId(null);
          setEntities(
            _.remove(entities, (entity) => entity.id !== activeEntityId)
          );
        })
        .catch((err) => {
          console.error(err);
        });
    }
  };

  return (
    <React.Fragment>
      <div className={styles.Block}>
        <h2 className={styles.BlockHeader}>
          {getFallbackValue(header, "Entity")}
        </h2>
        <div className={`${styles.Container} ${isPageble ? styles.PagebleContainer : ''}`}>
          <table className={styles.Table}>
            <thead>
              <tr>
                {columns.map((column, index) => {
                  return <th key={index}>{column.getTitle()}</th>;
                })}
              </tr>
            </thead>
            <tbody>
              {entities.map((entity) => {
                const setEntityActive = () => {
                  if (entity.id === activeEntityId) {
                    setActiveEntityId(null);
                    return;
                  }
                  setActiveEntityId(entity.id);
                };

                const entityData = convertEntityToSchema(entity, columns);

                return (
                  <AdminEntity
                    click={setEntityActive}
                    isActive={activeEntityId === entity.id ? true : false}
                    key={entity.id}
                    data={entityData}
                  />
                );
              })}
            </tbody>
          </table>
          {entities.length === 0 && (
            <div>{getFallbackValue(emptyMessage, "Entities not found")}</div>
          )}
        </div>
        <div className={styles.Buttons}>
          <AppButton link={newLink}>
            {getFallbackValue(newButtonText, "Create entity")}
          </AppButton>
          {isPageble && (
            <AppButton onClick={loadNewPage}>Load more...</AppButton>
          )}
          {activeEntityId && (
            <React.Fragment>
              <AppButton onClick={onEditClick}> Edit entity</AppButton>
              <AppButton onClick={onDeleteClick}> Delete entity</AppButton>
            </React.Fragment>
          )}
        </div>
      </div>
    </React.Fragment>
  );
};

const getFallbackValue = (obj, fallback) => {
  return obj ? obj : fallback;
};

const convertEntityToSchema = (entity, schema) => {
  let convertedEntity = {};

  for (let i = 0; i < schema.length; i++) {
    const schemaProp = schema[i].getField();
    const schemaFilter = schema[i].getFilter();
    if (Object.prototype.hasOwnProperty.call(entity, schemaProp)) {
      convertedEntity[schemaProp] = {
        value: entity[schemaProp],
        filter: schemaFilter,
      };
    }
  }
  return convertedEntity;
};

const ColumnName = (field, title, filter = (v) => v) => {
  if (!_.isString(field)) {
    throw new Error("[ColumnName]: Field is not string!");
  }

  if (!_.isString(title)) {
    throw new Error("[ColumnName]: Title is not string!");
  }

  if (filter !== null && !_.isFunction(filter)) {
    throw new Error("[ColumnName]: Filter is not function!");
  }

  const getField = () => {
    return field;
  };

  const getTitle = () => {
    return title;
  };

  const getFilter = () => {
    return filter;
  };

  return {
    getField,
    getTitle,
    getFilter,
  };
};

export { ColumnName };

export default AdminEntityTable;
