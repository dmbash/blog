import styles from "../styles/AppHeader.module.css";
import React from "react";
import Logo from "./Logo";
import AppHeaderLink from "./AppHeaderLink";

export default class AppHeader extends React.Component {
  constructor() {
    super();

    this.state = {
      collapseVisible: false,
    };

    this.toggleCollapse = this.toggleCollapse.bind(this);
  }

  toggleCollapse() {
    this.setState({
      collapseVisible: !this.state.collapseVisible,
    });
  }

  render() {
    return (
      <header className={styles.AppHeader}>
        <Logo className={styles.Logo} />
        <div className={styles.Title}>Блог - обо всём на свете</div>
        <button
          display="none"
          onClick={this.toggleCollapse}
          className={styles.HamburgerMenu}
        >
          Menu
        </button>
        <div
          className={`${styles.LinksWrapper} ${
            this.state.collapseVisible ? styles["LinksWrapper--toggled"] : ""
          }`}
        >
          <AppHeaderLink url="/cycles">Циклы</AppHeaderLink>
          <AppHeaderLink url="/themes">Темы</AppHeaderLink>
          <AppHeaderLink url="/about">Обо мне</AppHeaderLink>
        </div>
      </header>
    );
  }
}
