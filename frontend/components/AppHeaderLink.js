import React from "react";
import Link from "next/link";
import styles from "../styles/AppHeaderLink.module.css";

export default class AppHeaderLink extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { children, url, isActive } = this.props;

    return (
      <Link href={url} passHref>
        <a
          className={`${styles.Link} ${isActive ? styles["Link--active"] : ""}`}
          href={url}
        >
          {children}
        </a>
      </Link>
    );
  }
}
