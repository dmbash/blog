import React from "react";
import PropTypes from "prop-types";
import styles from "../styles/ArticleComment.module.css";

export default class ArticleComment extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { author, timestamp, text } = this.props;

    return (
      <div className={styles.Comment}>
        <div className={styles.HeaderContainer}>
          <div className={styles.Author}>{author}</div>
          <div className={styles.Timestamp}>{timestamp}</div>
        </div>
        <p className={styles.Text}>{text}</p>
      </div>
    );
  }
}

ArticleComment.propTypes = {
  author: PropTypes.string,
  timestamp: PropTypes.string,
  text: PropTypes.string,
};
