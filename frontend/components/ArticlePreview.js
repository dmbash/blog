import PropTypes from "prop-types";
import Link from "next/link";
import { BASE, API } from "../helpers/Network";
import React, { useEffect, useState } from "react";
import styles from "../styles/ArticlePreview.module.css";
import FetchedItem from '../components/FetchedItem';
import Tag from "./Tag";

const Article = (props) => {
  const { payload, isFeatured, id } = props;
  const link = BASE + "/article/" + id;

  let themes = [];
  if (payload.themes && payload.themes.length > 0) {
    for (var i = 0, len = payload.themes.length; i < len; i++) {
      themes.push(<Tag key={i} id={payload.themes[i].id} name={payload.themes[i].title} />);
    }
  }

  const cycleDOM = (payload.cycle && payload.cycle.title) ? ' / ' + payload.cycle.title : '';

 return (
   <div className={styles.Article}>
     <div
       className={`${styles.Cover} ${isFeatured ? styles.CoverFeatured : ""}`}
     >
       <img className={styles.CoverImage} src={payload.cover} />
     </div>
     <div
       className={`${styles.Container} ${
         isFeatured ? styles.ContainerFeatured : ""
       }`}
     >
       <div className={styles.Header}>
         <Link href={link}>
           <span>
             {payload.title}{cycleDOM}
           </span>
         </Link>
       </div>
       {isFeatured && (
         <div className={`${styles.Tags} ${styles.TagsFeatured}`}>{themes}</div>
       )}
       <div className={styles.Preview}>{payload.text}</div>
       {!isFeatured && <div className={styles.Tags}>{themes}</div>}
     </div>
   </div>
 );
}

const ArticlePreview = (props) => {
  const { id, isFeatured, payload } = props;

  const linkApi = API + "/article/" + id;
  
  return (
    <FetchedItem link={linkApi}>
      <Article isFeatured={isFeatured} id={id} />
    </FetchedItem>
  );
};

ArticlePreview.propTypes = {
  id: PropTypes.number.isRequired,
  isFeatured: PropTypes.bool,
};

export default ArticlePreview;
