import React from "react";
import styles from "../styles/ArticleViewCounter.module.css";
import PropTypes from "prop-types";

export default class ArticleViewCounter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { counter } = this.props;

    return <div className={styles.Counter}>{counter} views</div>;
  }
}

ArticleViewCounter.propTypes = {
  counter: PropTypes.number,
};
