import styles from "../styles/SectionDivider.module.css";
import React from "react";
import PropTypes from "prop-types";

export default class SectionDivider extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { name } = this.props;

    return <div className={styles.SectionDivider}>{name}</div>;
  }
}

SectionDivider.propTypes = {
  name: PropTypes.string,
};
