import axios from "axios";
    
const httpClient = axios.create();

const isBrowser = () => typeof window !== "undefined" 

httpClient.interceptors.request.use(function (config) {
    if (isBrowser()) {
      const token = localStorage.getItem('token');
      config.headers.Authorization =  token ? token : '';
    }
    
    return config;
});

httpClient.interceptors.response.use((response) => {
  return response;
}, (error) => {
  if (error.response.status === 401) {
    if (isBrowser()) {
      window.location = '/auth';
    } 
  }
  return error;
})

export function getData(url, config=null) {
  return httpClient.get(url, config)
    .then((res) => res.data)
    .catch((err) => console.error(err));
}


export default httpClient;
