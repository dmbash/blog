import '../styles/globals.css';
import React from 'react';
import Layout from '../components/Layout';

export default class MyApp extends React.Component {
  render () {
    const { Component, pageProps } = this.props
    return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
    )
  }
}
