import React, { useEffect, useState } from "react";
import Head from "next/head";
import styles from "../../styles/ArticlePage.module.css";
import { API } from "../../helpers/Network";
import SectionDivider from "../../components/SectionDivider";
import Tag from "../../components/Tag";
import UserGrade from "../../components/ArticleUserGrade";
import ViewCounter from "../../components/ArticleViewCounter";
import UserCommentary from "../../components/ArticleComment";
import AppButton from "../../components/AppButton";
import PropTypes from "prop-types";
import axios from "../../helpers/axios";
import Modal from 'react-modal';

const ArticlePage = (props) => {
  const { id } = props;

  let [article, setArticle] = useState({});
  let [isLoaded, setIsLoaded] = useState(false);
  let [error, setError] = useState(null);
  let [voted, setVoted] = useState(false);
  const [commentaryAuthor, setCommentaryAuthor] = useState('');
  const [commentaryBody, setCommentaryBody] = useState('');
  const [ isModalOpen, setIsModalOpen ] = useState(false);

  Modal.setAppElement('#__next');

  useEffect(() => {
    axios
      .get(`${API}/article/${id}`)
      .then((res) => {
        setIsLoaded(true);
        setArticle(res.data);
        axios.get(`${API}/article/${id}/view`);
      })
      .catch((err) => {
        setIsLoaded(true);
        setError(err);
      });
  }, []);

  const onLoadCommentsClick = (e) => {
    e.preventDefault();
    console.log("Load more comments... ");
  };

  const onLeaveCommentClick = (e) => {
    e.preventDefault();
    setIsModalOpen(true);
  };

  const closeModal = (e) => {
    e.preventDefault();
    setIsModalOpen(false);
  }

  const submitModal = (e) => {
    e.preventDefault();
    axios.post(`${API}/article/${id}/comment`, {author: commentaryAuthor, text: commentaryBody})
      .then((res) => {
        setIsModalOpen(false);
      })
      .catch((err) => console.error(err));
  }

  const onRatingPlus = (e) => {
    e.preventDefault();
    if (voted) return;
    axios
      .get(`${API}/article/${id}/vote/good`)
      .then((res) => {
        setVoted(true);
        const newArticle = { ...article };
        newArticle.rating.good += 1;
        setArticle(newArticle);
      })
      .catch((err) => console.error(err));
  };

  const onRatingMinus = (e) => {
    e.preventDefault();
    if (voted) return;
    axios
      .get(`${API}/article/${id}/vote/bad`)
      .then((res) => {
        setVoted(true);
        const newArticle = { ...article };
        newArticle.rating.bad += 1;
        setArticle(newArticle);
      })
      .catch((err) => console.error(err));
  };

  if (error) {
    return <div>Error while fetching: {error.message}</div>;
  }

  if (!isLoaded) {
    return <div>Loading...</div>;
  }

  let themes = [];

  if (article.themes && article.themes.length > 0) {
    themes = article.themes.map((tag, index) => {
      return <Tag id={tag.id} key={index} name={tag.title} />;
    });
  }

  const { comments } = article;

  let commentsArr = [];

  if (comments && comments.length > 0) {
    commentsArr = comments.map((comment, index) => {
      return (
        <UserCommentary
          key={index}
          author={comment.author}
          timestamp={comment.createdAt}
          text={comment.text}
        />
      );
    });
  }

  return (
    <div>
      <Head>
        <title>Article {id}</title>
      </Head>
      <section className={styles.ArticleSection}>
        <div className={styles.Cover}>
          <img className={styles.CoverImage} src={article.cover} />
        </div>
        <div className={styles.HeaderContainer}>
          <div className={styles.Row}>
            <div className={styles.Title}>{article.title}
            {article.cycle && (
              <div className={styles.Cycle}>
                <span>/</span>
                <div className={styles.Cycle}>{article.cycle.title}</div>
              </div>
            )}
            </div>
            <div className={styles.ArticleTimestamp}>{article.createdAt}</div>
          </div>
          <div className={styles.Row}>
            <div className={styles.TagContainer}>{themes}</div>
            <UserGrade
              good={article.rating ? article.rating.good : 0}
              bad={article.rating ? article.rating.bad : 0}
            />
            <ViewCounter counter={article.views && article.views.count} />
          </div>
        </div>
        <div className={styles.Line} />
        <div className={styles.ArticleText}>{article.text}</div>
      </section>
      <div className={styles.RatingControl}>
        <AppButton onClick={onRatingPlus}>+</AppButton>
        <AppButton onClick={onRatingMinus}>-</AppButton>
      </div>
      <SectionDivider name="Комментарии" />
      <section className={styles.CommentSection}>
        <div className={styles.Comments}>
          {commentsArr}
          {commentsArr.length === 0 && (
            <div className={styles.NoComments}>
              Комментарии отсутствуют, будьте первым (:{" "}
            </div>
          )}
        </div>
        <div className={styles.CommentButtons}>
          {commentsArr.length !== 0 ? (
            <AppButton onClick={onLoadCommentsClick} className={styles.Button}>
              Загрузить ещё
            </AppButton>
          ) : null}
          <AppButton
            onClick={onLeaveCommentClick}
            className={styles.Button}
            text=""
          >
            Оставьте комментарий
          </AppButton>
        </div>
        <Modal
          isOpen={isModalOpen}
          onRequestClose={closeModal}
          contentLabel="Оставить комментарий"
        >
          <div className={styles.ModalHeader}>
            <h2>Отправка комментария</h2>
            <AppButton onClick={closeModal}>x</AppButton>
          </div>
          <form>
            <label>
              Имя:
              <input 
                onChange={(e) => setCommentaryAuthor(e.target.value)}
              name="author" />
            </label>
            <label>
              Отзыв:
              <textarea 
              onChange={(e) => setCommentaryBody(e.target.value)}
              name="body" />
            </label>
            <AppButton onClick={submitModal}>Отправить комментарий</AppButton>
          </form>
        </Modal>
      </section>
    </div>
  );
};

ArticlePage.propTypes = {
  article: PropTypes.object,
  comments: PropTypes.array,
};

export async function getServerSideProps(context) {
  const { id } = context.query;
  return { props: { id } };
}

export default ArticlePage;
