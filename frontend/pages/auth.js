import React, { useState } from 'react';
import axios from '../helpers/axios';
import styles from '../styles/AuthPage.module.css';
import { useRouter } from 'next/router';
import { API } from '../helpers/Network';

const Auth = (props) => {
  let [ username, setUsername ] = useState('');
  let [ password, setPassword ] = useState('');
  let [ msg, setMsg ] = useState('');
  let [ error, setError ] = useState(null);

  const router = useRouter();

  const onAuthClick = (e) => {
    e.preventDefault(); 

    if ( username === '' )
      return;
    if (password === '')
      return;

    axios.post(`${API}/auth`, {username, password})
      .then((res) => {
        setError(null);
        setMsg("Успешно");
        localStorage.setItem('token', res.data);
        setTimeout(() => {
            router.push('/');
        }, 500)
      })
      .catch((err) => {
        setError(err);
      });
  }

  const onLoginChange = (e) => {
    e.preventDefault(); 
    setUsername(e.target.value);
  }

  const onPasswordChange = (e) => {
    e.preventDefault(); 
    setPassword(e.target.value);
  }


  return <section className={styles.Section}>
    <h1>Авторизация</h1>
    <form className={styles.Form}>
      <label for="login">
        Логин: 
        <input required name="login" onChange={onLoginChange} type="text" />
      </label>
      <label for="password">
        Пароль:
        <input required name="password" onChange={onPasswordChange} type="password" />
      </label>
      <button onClick={onAuthClick} type="submit">Авторизоваться</button>
    </form>
    { msg && msg }
    { error && error.message }
    </section>
}

export async function getServerSideProps(context) {
  return { props: {} }
}

export default Auth;
