import React, { useState } from 'react';
import styles from '../styles/AuthPage.module.css';
import { API } from '../helpers/Network';

const Signup = (props) => {
  let [ username, setUsername ] = useState('');
  let [ password, setPassword ] = useState('');
  let [ msg, setMsg ] = useState('');
  let [ error, setError ] = useState(null);

  const axios = require("axios");

  const onAuthClick = (e) => {
    e.preventDefault(); 

    if ( username === '' )
      return;
    if (password === '')
      return;

    const link = API + "/signup";


    axios.post(link, { username, password })
      .then((res) => {
        console.log(res);
        setError(null);
        setMsg(res);
      })
      .catch((err) => {
        setError(err);
      });
  }

  const onLoginChange = (e) => {
    e.preventDefault(); 
    setUsername(e.target.value);
  }

  const onPasswordChange = (e) => {
    e.preventDefault(); 
    setPassword(e.target.value);
  }


  return <section className={styles.Section}>
    <h1>Регистрация</h1>
    <form className={styles.Form}>
      <label for="login">
        Логин: 
        <input required name="login" onChange={onLoginChange} type="text" />
      </label>
      <label for="password">
        Пароль:
        <input required name="password" onChange={onPasswordChange} type="password" />
      </label>
      <button onClick={onAuthClick} type="submit">Зарегистрироваться</button>
    </form>
    { msg && msg.data }
    { error && error.message}
    </section>
}

export async function getServerSideProps(context) {
  return { props: {} }
}

export default Signup;
