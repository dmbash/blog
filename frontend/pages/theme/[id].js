import React from 'react';
import { API } from '../../helpers/Network';
import PropTypes from 'prop-types';
import ArticlePreview from '../../components/ArticlePreview';
import SectionDivider from '../../components/SectionDivider';

const ThemePage = (props) => {

  const { theme } = props;

  let articles = [];

  console.log(theme);

  for (var i = 0, len = theme.articles.length; i < len; i++) {
    articles.push(<ArticlePreview key={i} id={theme.articles[i].id} />);
  }
  
  return (
    <section>
      <h1>{theme.title}</h1>
      <SectionDivider name="Публикации" />
      <section>
        { theme.articles.length === 0 && (
          <div className="Error">С данной темой публикаций нет</div>
        ) }
        {articles}
      </section>
    </section>
  )
}

export async function getServerSideProps(context) {
  const res = await fetch(API + '/theme/' + context.query.id) 
  const theme = await res.json();
  return { props: { theme } }
}

ThemePage.protoTypes = {
  articleIds: PropTypes.array,
  theme: PropTypes.object,
};

export default ThemePage;
